package mpci0022MV.model;

import mpci0022MV.util.Validator;
import org.junit.*;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {

     Carte c;
     Carte c1;
     Carte b;


    @Before
    public void setUp() throws Exception {

        c1 = new Carte();
        c = new Carte();
        b = new Carte();

        c.setTitlu("Povestiri");
        c.setEditura("Litera");
        c.setAnAparitie("1900");

        ArrayList<String> autori = new ArrayList<String>();
        autori.add("M");
        autori.add("A");
        c.setAutori(autori);

        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("Mara");
        cuvinteCheie.add("M3m3");
        c.setCuvinteCheie(cuvinteCheie);
        System.out.println("-----beforeTest-----");


    }


    @BeforeClass
    public static void setup() {

        System.out.println("before any test");
    }
    @Test(timeout=100)
    public void getEditura() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(c.getEditura(), "Litera");
    }
    @Test(timeout=100)
    public void getCuvinteCheieTo(){
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertFalse(c.getCuvinteCheie().isEmpty());
        System.out.println("getCuvinteCheie nu este gol!");


    }
    @Test
    public void getAutori(){

        assertTrue(c.getAutori().contains("M") && c.getAutori().contains("A"));
        System.out.println("Au fost identificate valorile corecte in getAutori()");
    }


    /*@Test(expected = NullPointerException.class)
    public void testConstructor(){

        assert c1 == null;
        throw new NullPointerException();
    }*/

    @AfterClass
    public static void teardown() {

        System.out.println("after all tests");
    }

    @After
    public void tearDown() throws Exception {

        System.out.println("-----afterTest-----");
    }



    @Test
    public void getTitlu() {

        assertTrue(c.getTitlu() == "Povestiri");
        assertEquals("Povestiri", c.getTitlu());

    }

    @Test(timeout=100)
    public void getAnAparitieTest(){
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertTrue(c.getAnAparitie() == "1900");
        System.out.println("getAnAparitieTest functioneaza");

    }
    @Test
    public void getAnAparitie() {

        assertTrue(c.getAnAparitie() == "1900");
        System.out.println("assertTrue getAnAparitie");
    }


    @Test
    public void getCuvinteCheie() throws Exception{

        if(c.getCuvinteCheie().isEmpty()){

            throw new Exception("Cuvintele cheie nu sunt adaugate");
        }

        assertFalse(c.getCuvinteCheie().isEmpty());
        System.out.println("getCuvinteCheie nu este gol, adaugarea a fost efectuata");
    }




}